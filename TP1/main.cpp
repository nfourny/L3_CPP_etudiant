#include <iostream>
#include "fibonnaci.hpp"

int main() {
	std::cout << fibonacciRecursif(7) << std::endl;
	std::cout << fibonacciIteratif(7) << std::endl;
}