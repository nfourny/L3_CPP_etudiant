using namespace std;
#include <iostream>
#include "fibonnaci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonnaci){};

TEST(GroupFibonnaci, test_rec) {
	CHECK_EQUAL(0,fibonacciRecursif(0));
	CHECK_EQUAL(1,fibonacciRecursif(1));
	CHECK_EQUAL(1,fibonacciRecursif(2));
	CHECK_EQUAL(2,fibonacciRecursif(3));
	CHECK_EQUAL(3,fibonacciRecursif(4));
}

TEST(GroupFibonnaci, test_ite) {
	CHECK_EQUAL(0,fibonacciIteratif(0));
	CHECK_EQUAL(1,fibonacciIteratif(1));
	CHECK_EQUAL(1,fibonacciIteratif(2));
	CHECK_EQUAL(2,fibonacciIteratif(3));
	CHECK_EQUAL(3,fibonacciIteratif(4));
}